FROM debian:10.4-slim AS base

WORKDIR /app

COPY mytemplate.tex mytemplate.tex
COPY meta.yaml meta.yaml
COPY acmart.cls acmart.cls
COPY template.md template.md

RUN apt-get -y update && apt-get -y upgrade &&\
        apt-get -y install texlive-xetex pandoc yq

FROM base

COPY make-report.sh make-report.sh

ENTRYPOINT [ "bash", "make-report.sh" ]

# curl https://raw.githubusercontent.com/kikofernandez/pandoc-examples/master/research-paper/mytemplate.tex
# https://raw.githubusercontent.com/kikofernandez/pandoc-examples/master/research-paper/meta.yaml
# https://raw.githubusercontent.com/kikofernandez/pandoc-examples/master/research-paper/main.md
# https://raw.githubusercontent.com/kikofernandez/pandoc-examples/master/research-paper/acmart.cls
