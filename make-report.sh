#!/usr/bin/env bash
set -eEu -o pipefail

reportdata_yaml=$(mktemp)

cat - | yq r -P - > "$reportdata_yaml"

input=template.md
output="$(mktemp)"

pandoc "$input" \
       --pdf-engine=xelatex \
       -o "$output" \
       --table-of-contents \
       --number-sections  \
       ----metadata-file "$reportdata_yaml" \
       --from markdown+yaml_metadata_block+raw_html

cat $output
